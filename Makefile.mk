SDK:=$(abspath $(lastword $(MAKEFILE_LIST)/..))
PRU_CGT:=/usr/share/ti/cgt-pru
PRU_SUPPORT:=/usr/lib/ti/pru-software-support-package

LD=lnkpru
LDFLAGS=--reread_libs --warn_sections --stack_size=0x100 --heap_size=0x100 \
 -i$(PRU_CGT)/lib -i$(PRU_CGT)/include $(SDK)/am335x_pru.cmd --library=libc.a \
 --library=$(PRU_SUPPORT)/lib/rpmsg_lib.lib
CC=clpru
CFLAGS=--include_path $(PRU_SUPPORT)/include \
 --include_path $(PRU_SUPPORT)/include/am335x \
 --include_path $(PRU_CGT)/include \
 --include_path $(SDK) \
 -v3 -O2 --printf_support=minimal --display_error_number --endian=little --hardware_mac=on \
 -ppd -ppa --asm_listing --keep_asm

%.out: %.obj
	$(LD) -o $@ $< $(LDFLAGS) -m $(basename $@).map

%.obj: %.c
	$(CC) $(CFLAGS) --output_file $@ -c $^
