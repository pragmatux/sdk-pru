sdk-pru
=======

Notes
-----

Include /usr/share/sdk-pru/Makefile.mk at the top of your Makefile.

See Also
---------

"PRU Optimizing C/C++ Compiler v2.3" -- July 2018 (c) Texas Instruments, Inc.

